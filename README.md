How to use ?

1 - create button with class ".cs-toggler".
2 - link css and js assets of the plugin (country-selector.css && cs.js);
3 - use plugin as below : 

$(".cs-toggler").cs({
        code: 'EG',
        countryTargetInput: '.country',
        background: 'transparent',
        codeInput: '#code'
});

Don't forget to include "data.json" file in the same plugin folder.

